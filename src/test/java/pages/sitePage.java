package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import testcases.verifyReg;

import java.util.Random;

public class sitePage {




    WebDriver driver;

    By username=By.id("registrationform-name");
    By useremail=By.id("registrationform-email");
    By checkbox = By.className("form__checkbox-view");
    By popup2 = By.cssSelector(".cook-popup__button:nth-child(3)");
    By popup = By.cssSelector(".conditions__list > li:nth-child(5)");
    By openaccount=By.cssSelector("#registrationForm > div.form__submit-group > button");




    public sitePage(WebDriver driver)

    {

           this.driver=driver;



    }

    public void typeUserName()

    {
       driver.findElement(username).sendKeys("testuser");
    }

    public void typeUserEmail()

    {

       driver.findElement(useremail).sendKeys("six.testemail+" + Math.floor(Math.random()*11111) + "@gmail.com");
       System.out.println(useremail);
    }

    public void clickCheckBox() {driver.findElement(checkbox).click();}

    public void clickPopUp()

    {


        WebDriverWait wait =new WebDriverWait(driver, 900);
        driver.findElement(popup2).click();
        wait.until(ExpectedConditions.elementToBeClickable(openaccount));
        driver.findElement(popup).click();
       wait.until(ExpectedConditions.elementToBeClickable(openaccount));
        System.out.println("pop up clicked");



    }

    public void clickOnOpen()

    {

    try {

        driver.findElements(openaccount).get(0).click();
        WebDriverWait wait = new WebDriverWait(driver, 900);
        wait.until(ExpectedConditions.elementToBeClickable(openaccount));
        driver.findElements(openaccount).get(0).click();
        wait.until(ExpectedConditions.elementToBeClickable(openaccount));
        System.out.println("clicked sucess");
        Assert.assertEquals("https://www.fbs.com/trading/standard", "https://www.fbs.com/trading/standard","Redirected");
    }
    catch (NoSuchElementException e) {

        System.out.println("Register element is not found.");

        throw(e);

    } catch (TimeoutException e1) {

        System.out.println("Timeout element is not found.");

        throw(e1);

    }
}

    public void waiter() throws InterruptedException

    {
        try{
            Thread.sleep(500000);
        }
        catch(InterruptedException ie){
        }
    System.out.println("waiiiit please");


}


}

