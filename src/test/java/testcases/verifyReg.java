package testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import pages.sitePage;
import pages.webDriver;

public class verifyReg extends webDriver {



    @Test
    public void verifyRegistration() throws InterruptedException {

        driver.navigate().to("https://www.fbs.com/trading/standard");

        sitePage registration = new sitePage(driver);

        //registration.clickPopUp();
        registration.typeUserName();
        registration.typeUserEmail();
        registration.clickCheckBox();
        registration.clickOnOpen();
        registration.waiter();

        driver.quit();
    }

    }


